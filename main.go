package main

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"github.com/gorilla/mux"
	"golang.org/x/sync/errgroup"
)

func handler(w http.ResponseWriter, r *http.Request) {
	g, _ := errgroup.WithContext(context.Background())

	data1 := map[string]interface{}{
		"BeneficiaryAddress": "tes",
		"BeneficiaryName":    "tes",
		"PaymentDate":        "01-02-1997",
		"Contact":            "alhamsya",
		"BankNumber":         "081212",
		"BankAccount":        "1",
		"BIC":                "12",
	}

	g.Go(func() error {
		_, err := Generate("example", "example.html", data1)
		if err != nil {
			return err
		}

		return nil
	})

	data2 := map[string]interface{}{
		"BeneficiaryAddress": "tes2",
		"BeneficiaryName":    "tes2",
		"PaymentDate":        "01-02-1997",
		"Contact":            "alhamsya2",
		"BankNumber":         "081212",
		"BankAccount":        "12",
		"BIC":                "12",
	}

	g.Go(func() error {
		_, err := Generate("form", "form.html", data2)
		if err != nil {
			return err
		}

		return nil
	})

	if err := g.Wait(); err != nil {
		log.Println(err)
	}

	w.Write([]byte(fmt.Sprintf("Hello, %s\n", "test")))
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func readinessHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func main() {
	// Create Server and Route Handlers
	r := mux.NewRouter()

	r.HandleFunc("/", handler)
	r.HandleFunc("/health", healthHandler)
	r.HandleFunc("/readiness", readinessHandler)

	srv := &http.Server{
		Handler:      r,
		Addr:         ":8080",
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	// Start Server
	go func() {
		log.Println("Starting Server")
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	// Graceful Shutdown
	waitForShutdown(srv)
}

func waitForShutdown(srv *http.Server) {
	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// Block until we receive our signal.
	<-interruptChan

	// create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	srv.Shutdown(ctx)

	log.Println("Shutting down")
	os.Exit(0)
}

//Generate html to PDF
func Generate(prefix, filename string, dataBody map[string]interface{}) (string, error) {
	var (
		outputPath   = "files/temp-directory"
		templatePath = "files/template-pdf"
	)

	// check folder template HTML to PDF
	if _, err := os.Stat(templatePath); os.IsNotExist(err) {
		templatePath = "./"
	}

	// check folder temporary
	if _, err := os.Stat(outputPath); os.IsNotExist(err) {
		outputPath = "./"
	}

	prefix = strings.Replace(prefix, "/", "_", -1)

	PrintMemUsage()
	pdfG, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		return "", err
	}

	PrintMemUsage()

	buffTemplate, err := parseTemplate(fmt.Sprintf("%s/%s", templatePath, filename), dataBody)
	if err != nil {
		return "", err
	}

	PrintMemUsage()

	buf := new(bytes.Buffer)

	buf.Write(buffTemplate)
	buf.WriteString(`<P style="page-break-before: always">`)
	buf.Write(buffTemplate)
	buf.WriteString(`<P style="page-break-before: always">`)
	buf.Write(buffTemplate)

	pdfG.AddPage(wkhtmltopdf.NewPageReader(buf))
	PrintMemUsage()

	pdfG.Orientation.Set(wkhtmltopdf.OrientationPortrait)
	PrintMemUsage()

	pdfG.Dpi.Set(300)
	PrintMemUsage()

	if err := pdfG.Create(); err != nil {
		return "", err
	}
	PrintMemUsage()

	outputFile, err := ioutil.TempFile(outputPath, fmt.Sprintf("%s-*.pdf", prefix))
	if err != nil {
		return "", err
	}
	PrintMemUsage()

	if err := pdfG.WriteFile(outputFile.Name()); err != nil {
		return "", err
	}
	PrintMemUsage()

	return outputFile.Name(), nil
}

//parseTemplate send data to HTML
func parseTemplate(filename string, data interface{}) ([]byte, error) {
	tempFile, err := template.ParseFiles(filename)
	if err != nil {
		return nil, err
	}

	result := new(bytes.Buffer)
	if err := tempFile.Execute(result, data); err != nil {
		return nil, err
	}

	return result.Bytes(), nil
}

func PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
	fmt.Printf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
	fmt.Printf("\tSys = %v MiB", bToMb(m.Sys))
	fmt.Printf("\tNumGC = %v\n", m.NumGC)
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
