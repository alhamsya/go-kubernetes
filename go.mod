module github.com/alhamsya/go-kubernetes

go 1.18

require (
	github.com/SebastiaanKlippert/go-wkhtmltopdf v1.7.2
	github.com/gorilla/mux v1.8.0
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f
)
