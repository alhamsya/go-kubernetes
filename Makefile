run-docker:
	@printf "\033[0;30m\033[42m === RUN DOCKER === \033[0m\n"
	@docker build --platform linux/x86_64 -t alhamsya/go-app-img .
	@docker run -d -p 3333:8080 --name go-app-container --memory="200Mi" alhamsya/go-app-img

build-ci:
	@GOOS=linux GOARCH=amd64
	@echo ">> Building GRPC..."
	@GOOS=linux GOARCH=amd64 CGO_ENABLED=0  go build -o ./bin/REST ./app/rest
	@echo ">> Finished"