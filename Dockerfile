FROM golang:latest as build
WORKDIR /app
COPY . .
RUN go build -o bin/service -ldflags="-s -w" main.go

# Image
FROM registry.gitlab.com/stockbit.com/microapi/trading-process-rdn-bca/wkhtmltopdf:0.12.6.1-2 as wkhtmltopdf

FROM alpine:3.15

# Alpine 3.11 and higher versions have libstdc++ v9+ in their repositories which breaks the build
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.10/main' >> /etc/apk/repositories

RUN apk add --no-cache \
  libstdc++=8.3.0-r0 \
  libx11 \
  libxrender \
  libxext \
  libssl1.1 \
  ca-certificates \
  fontconfig \
  freetype \
  ttf-dejavu \
  ttf-droid \
  ttf-freefont \
  ttf-liberation \
  ttf-ubuntu-font-family \
&& apk add --no-cache --virtual .build-deps \
  msttcorefonts-installer \
\
# Install microsoft fonts
&& update-ms-fonts \
&& fc-cache -f \
\
# Clean up when done
&& rm -rf /tmp/* \
&& apk del .build-deps \
&& apk add --no-cache ca-certificates tzdata \
&& cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime\
&& echo "Asia/Jakarta" >  /etc/timezone \
&& addgroup -g 1001 -S app && adduser -u 1001 -S app  -G app

USER app

WORKDIR /app/

COPY --from=build /app/bin/service .
COPY --from=wkhtmltopdf /bin/wkhtmltopdf /bin/wkhtmltopdf
ADD ./files ./files/
CMD ["./service"]